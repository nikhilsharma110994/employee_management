import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'dockabl';
  panelOpenStateFirst = true;
  panelOpenStateSecond = false;
  panelOpenStateThird = false;
  submitForm: boolean = false;
  private startDate: any;
  private summary_text: string = 'SUMMARY';
  private ownerType: string = '0';
  private alignmentType: string = 'func';
  private alignmentSub = { "func": ['Engineering', 'Design', 'Sales'], "bsc": ['Business', 'People', 'Process', 'Customer'] };

  myControl = new FormControl();
  options: string[] = ['Nikhil', 'Anmol', 'Archit', 'Sushant', 'Chirag'];
  filteredOptions: Observable<string[]>;

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  minDate = new Date();
  maxDate = new Date();

  toFilter = (d: Date): boolean => {
    const diff = d.valueOf() - this.startDate.valueOf();
    if (diff < 0) {
      return false;
    }
    const diffDays: any = Math.ceil(diff / (1000 * 3600 * 24));
    return diffDays <= 365;
  }

  checkValidation(form: any) {
    console.log(form);
    if(form.form.valid == true) {
      this.summary_text = 'FORM IS VALID';
      this.submitForm = true;
    }
  }
}
